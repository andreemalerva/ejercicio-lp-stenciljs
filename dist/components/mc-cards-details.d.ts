import type { Components, JSX } from "../types/components";

interface McCardsDetails extends Components.McCardsDetails, HTMLElement {}
export const McCardsDetails: {
  prototype: McCardsDetails;
  new (): McCardsDetails;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
