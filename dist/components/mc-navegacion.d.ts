import type { Components, JSX } from "../types/components";

interface McNavegacion extends Components.McNavegacion, HTMLElement {}
export const McNavegacion: {
  prototype: McNavegacion;
  new (): McNavegacion;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
