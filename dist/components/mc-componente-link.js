import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const componenteLinkCss = ".link-boton{background-image:url('../assets/images/bg-boost-desktop.svg');background-color:#3a3053;background-size:cover;background-position:center center;padding-bottom:5%}.link-boton h2{padding:4% 0 3%;margin-block-start:0;margin-block-end:0;text-align:center;font-family:var(--font1);font-size:40px;line-height:36px;font-weight:bold;color:#fff}.link-boton a{border-radius:30px;padding:12px 0;background-color:#2ad0d3;display:flex;justify-content:center;align-items:center;width:15%;font-family:var(--font1);font-size:20px;font-weight:bold;color:#fff;text-decoration:none;margin:0 auto}@media screen and (max-width: 980px){.link-boton a{width:40%}}@media screen and (max-width: 425px){.link-boton{padding-top:10%;padding-bottom:12%}.link-boton a{width:40%;margin:10% auto 5%}}";

const ComponenteLink = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "link-boton" }, h("h2", null, " Boost your links today "), h("a", { href: "#" }, "Get Started")));
  }
  static get style() { return componenteLinkCss; }
}, [1, "mc-componente-link"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-componente-link"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-componente-link":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, ComponenteLink);
      }
      break;
  } });
}

const McComponenteLink = ComponenteLink;
const defineCustomElement = defineCustomElement$1;

export { McComponenteLink, defineCustomElement };
