import type { Components, JSX } from "../types/components";

interface McCardsDetailsTarjeta extends Components.McCardsDetailsTarjeta, HTMLElement {}
export const McCardsDetailsTarjeta: {
  prototype: McCardsDetailsTarjeta;
  new (): McCardsDetailsTarjeta;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
