import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const navegacionCss = ":host(){margin:0}.topnav{overflow:hidden;background-color:#ffff;display:flex;justify-content:space-between;padding:2.5% 10% 1%}.topnav a{padding:14px 0;font-family:var(--font1);font-size:17px;font-weight:bold;color:#a1a1a3;text-decoration:none}.topnav a img{vertical-align:middle}.nav-izquierda{display:flex;align-items:center;justify-content:space-between;gap:30px}.menuContent{display:flex;align-items:center;justify-content:space-between;gap:25px}.menuContent a:hover{background-color:rgba(221, 221, 221, 0);color:black}.menuContent a.active{background-color:#f0e914;color:white}.menuContent .icon{display:none}#contacto{background-color:#2AD0D3;border-radius:35px;color:#fff;font-family:var(--font1);text-transform:capitalize;padding:8px 20px}@media screen and (max-width: 600px){.menuContent a,.menuContent a:not(:first-child){display:none}.menuContent a.icon{float:right;display:block}.menuContent .icon:hover{background-color:#000000}.menuContent .icon:active{background-color:#000000}.menuContent.responsive{position:absolute;background-color:#000000;display:inline-block;width:100vw;text-align:center}.menuContent.responsive .icon{position:absolute;right:0;top:0}.menuContent.responsive a{float:none;display:block;text-align:left;color:rgb(255, 255, 255)}.fa-navicon:before,.fa-reorder:before,.fa-bars:before{content:\"\\f0c9\";color:white}}";

const Navegacion = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "topnav" }, h("div", { class: "nav-izquierda" }, h("a", { href: "#home" }, h("img", { src: "./assets/images/logo.svg", alt: "Logotipo" })), h("a", { href: "#about" }, "Feature"), h("a", { href: "#about" }, "Pricing"), h("a", { href: "#services" }, "Resources")), h("div", { class: "menuContent", id: "myTopnav" }, h("a", { href: "#proyects" }, "Login"), h("a", { href: "#contact", id: "contacto" }, "Sign Up"), h("a", { href: "#", class: "icon" }, h("i", { class: "fa fa-bars" })))));
  }
  static get style() { return navegacionCss; }
}, [1, "mc-navegacion", {
    "opened": [1540]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-navegacion"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-navegacion":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Navegacion);
      }
      break;
  } });
}

const McNavegacion = Navegacion;
const defineCustomElement = defineCustomElement$1;

export { McNavegacion, defineCustomElement };
