import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const getStartedCss = ".caja-component{background-image:url('../assets/images/bg-shorten-desktop.svg');background-color:#3A3053;background-size:cover;background-position:center center;margin:0 10%;height:150px;border-radius:10px;display:flex;justify-content:center;align-items:center;gap:25px;position:absolute;right:0;left:0;z-index:1000}.caja-component input{margin-left:3.5%;border-radius:10px;padding:12px 16px;background-color:#ffff;width:90%;border:1px solid #fff;color:#a1a1a3;font-family:var(--font1);font-size:17px;font-weight:200}.caja-component input::placeholder{color:#a1a1a3;font-family:var(--font1);font-size:17px;font-weight:200}.caja-component input:focus{outline:none}.caja-component a{margin-right:3.5%;border-radius:10px;padding:12px 16px;background-color:#2AD0D3;width:10%;font-family:var(--font1);font-size:17px;font-weight:bold;color:#fff;text-decoration:none;text-align:center}@media screen and (max-width: 1024px){.caja-component{margin:0 15px}.caja-component input{width:80%}.caja-component a{width:20%}}@media screen and (max-width: 767px){.caja-component{margin:0 15px;gap:15px}.caja-component input{width:60%}.caja-component input::placeholder{font-size:12px;font-weight:200}.caja-component a{width:40%;padding:13px 5px;font-size:14px}}@media screen and (max-width: 425px){.caja-component{flex-flow:wrap;padding:15px;gap:0;height:136px}.caja-component input{width:100%;margin-left:0}.caja-component input::placeholder{font-size:12px;font-weight:200}.caja-component a{width:100%;padding:13px 5px;font-size:14px;margin-right:0}}";

const GetStarted = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "caja-component" }, h("input", { type: "text", placeholder: "Shorten a link here..." }), h("a", { href: "#" }, "Shorten In!")));
  }
  static get style() { return getStartedCss; }
}, [1, "mc-get-started"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-get-started"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-get-started":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, GetStarted);
      }
      break;
  } });
}

const McGetStarted = GetStarted;
const defineCustomElement = defineCustomElement$1;

export { McGetStarted, defineCustomElement };
