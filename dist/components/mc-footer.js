import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const footerCss = ".footer{background-color:#232027;display:flex;justify-content:space-between;align-items:flex-start;padding:4% 10%}.links-footer{display:flex}.footer ul{list-style:none;margin-block-start:0}.footer ul p{color:#fff;font-size:16px;line-height:28px;font-weight:bold;font-family:var(--font1);margin-block-start:0}.footer ul li a{color:#a1a1a3;font-size:14px;line-height:30px;font-weight:300;font-family:var(--font1);text-decoration:none}.redes-sociales{display:flex;gap:25px}@media screen and (max-width: 980px){.footer{padding:4% 15px}.redes-sociales{display:flex;gap:15px}}@media screen and (max-width: 767px){.footer{flex-direction:column;padding:14% 15px;justify-content:center;align-items:center}.footer ul{padding-inline-start:0}.links-footer{flex-direction:column;text-align:center;margin:12% 0}.redes-sociales{display:flex;gap:15px}}";

const Footer = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "footer" }, h("slot", { name: "logotipo" }), h("div", { class: "links-footer" }, h("ul", null, h("p", null, "Features"), h("li", null, h("a", { href: "#" }, "Link Shortening")), h("li", null, h("a", { href: "#" }, "Branded Links")), h("li", null, h("a", { href: "#" }, "Analytics"))), h("ul", null, h("p", null, "Resources"), h("li", null, h("a", { href: "#" }, "Blog")), h("li", null, h("a", { href: "#" }, "Developers")), h("li", null, h("a", { href: "#" }, "Support"))), h("ul", null, h("p", null, "Company"), h("li", null, h("a", { href: "#" }, "About")), h("li", null, h("a", { href: "#" }, "Our Team")), h("li", null, h("a", { href: "#" }, "Careers")), h("li", null, h("a", { href: "#" }, "Contact")))), h("div", { class: "redes-sociales" }, h("slot", { name: "facebook" }), h("slot", { name: "twitter" }), h("slot", { name: "pinteres" }), h("slot", { name: "instagram" }))));
  }
  static get style() { return footerCss; }
}, [1, "mc-footer"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-footer"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-footer":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Footer);
      }
      break;
  } });
}

const McFooter = Footer;
const defineCustomElement = defineCustomElement$1;

export { McFooter, defineCustomElement };
