import type { Components, JSX } from "../types/components";

interface McHeader extends Components.McHeader, HTMLElement {}
export const McHeader: {
  prototype: McHeader;
  new (): McHeader;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
