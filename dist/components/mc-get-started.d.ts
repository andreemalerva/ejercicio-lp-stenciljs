import type { Components, JSX } from "../types/components";

interface McGetStarted extends Components.McGetStarted, HTMLElement {}
export const McGetStarted: {
  prototype: McGetStarted;
  new (): McGetStarted;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
