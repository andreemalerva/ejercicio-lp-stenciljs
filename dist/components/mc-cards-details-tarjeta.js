import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const tarjetaCss = ".contenido .icon{width:80px;height:80px;border-radius:50%;background-color:#3a3053;display:flex;justify-content:center;align-items:center;margin:-50px auto 0}.contenido img{width:40px;height:40px;vertical-align:middle}.contenido h4{font-family:var(--font1);font-size:20px;line-height:24px;font-weight:bold;color:#34313c;margin-block-end:0.33em}.contenido p{font-family:var(--font1);font-size:16px;line-height:28px;font-weight:400;color:#a1a1a3}";

const Tarjeta = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "contenido" }, h("div", { class: "icon" }, h("img", { src: this.icono, alt: "icono" })), h("h4", null, this.titulo), h("p", null, this.parrafo)));
  }
  static get style() { return tarjetaCss; }
}, [1, "mc-cards-details-tarjeta", {
    "icono": [1],
    "titulo": [1],
    "parrafo": [1]
  }]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-cards-details-tarjeta"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-cards-details-tarjeta":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Tarjeta);
      }
      break;
  } });
}

const McCardsDetailsTarjeta = Tarjeta;
const defineCustomElement = defineCustomElement$1;

export { McCardsDetailsTarjeta, defineCustomElement };
