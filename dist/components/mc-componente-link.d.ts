import type { Components, JSX } from "../types/components";

interface McComponenteLink extends Components.McComponenteLink, HTMLElement {}
export const McComponenteLink: {
  prototype: McComponenteLink;
  new (): McComponenteLink;
};
/**
 * Used to define this component and all nested components recursively.
 */
export const defineCustomElement: () => void;
