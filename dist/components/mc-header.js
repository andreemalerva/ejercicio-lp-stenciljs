import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const headerCss = "main{display:flex;justify-content:center;align-items:center;padding:0 0 0 10%;height:70vh}main h1{font-family:var(--font1);font-size:82px;line-height:96px;font-weight:bold;color:#34313c;margin-block-start:0;margin-block-end:0.5rem}main h2{font-family:var(--font1);font-size:24px;line-height:36px;font-weight:400;color:#a1a1a3;margin-block-start:0;margin-block-end:0;margin-bottom:7%}main a{background-color:#2ad0d3;border-radius:35px;color:#fff;font-size:20px;line-height:20px;font-family:var(--font1);text-transform:capitalize;padding:16px 0;text-decoration:none;display:flex;justify-content:center;align-items:center;width:30%}.derecha{background-image:url('./assets/images/illustration-working.svg');background-size:cover;height:60vh;width:100%}@media screen and (max-width: 1024px){main h1{font-size:42px;line-height:46px}main h2{font-size:18px;line-height:32px}main a{font-size:18px;line-height:20px;width:35%}}@media screen and (max-width: 920px){main{padding:0}main .izquierda{padding:0 15px}}@media screen and (max-width: 767px){main{flex:1;flex-flow:column wrap;height:100vh;justify-content:flex-start;align-items:flex-start}main h1{font-size:42px;line-height:46px;text-align:center}main h2{font-size:22px;line-height:30px;text-align:center}main a{font-size:18px;line-height:20px;margin:0 auto}main .izquierda{flex:1;order:2;padding:0 15px;margin:5% 0}main .derecha{order:1;height:300px}}@media screen and (max-width: 374px){main .derecha{height:250px}main a{width:50%}}";

const Header = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("main", null, h("div", { class: "izquierda" }, h("h1", null, "More than just shorter links"), h("h2", null, "Build your brand\u2019s recognition and get detailed insights on how your links are performing."), h("a", { href: "#" }, "Get Started")), h("div", { class: "derecha" })));
  }
  static get style() { return headerCss; }
}, [1, "mc-header"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-header"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-header":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, Header);
      }
      break;
  } });
}

const McHeader = Header;
const defineCustomElement = defineCustomElement$1;

export { McHeader, defineCustomElement };
