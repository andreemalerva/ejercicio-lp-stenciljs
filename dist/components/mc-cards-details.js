import { proxyCustomElement, HTMLElement, h } from '@stencil/core/internal/client';

const cardsDetailsCss = ".detalles{background-color:#f0f1f6;text-align:center;padding:13% 10% 8%;margin-top:80px}.detalles h2{font-family:var(--font1);font-size:40px;line-height:36px;font-weight:bold;color:#34313c;margin-block-end:0}.detalles h3{font-family:var(--font1);font-size:20px;line-height:36px;font-weight:400;color:#a1a1a3;text-align:center;padding:0 20%}.card-inline{display:flex;justify-content:space-between;align-items:center;gap:10px}.card-inline .linea{position:absolute;width:40%;height:7px;background-color:#2AD0D3;left:0;right:0;border:1px solid #2AD0D3;z-index:1}.tarjetas{background-color:#fff;width:27%;text-align:left;border-radius:10px;padding:12px 2%;z-index:10000}.tarjetas:nth-of-type(2){margin-top:6%}.tarjetas:nth-of-type(3){margin-top:12%}@media screen and (max-width: 1024px){.detalles h2{font-size:20px;line-height:32px}.detalles h3{font-size:18px;line-height:26px}}@media screen and (max-width: 425px){.detalles{padding:25% 15px}.card-inline{flex-direction:column}.tarjetas{width:90%;text-align:center;padding:12px 4%;margin:14% 15px}.tarjetas:nth-of-type(2){margin-top:0}.tarjetas:nth-of-type(3){margin-top:0}.card-inline .linea{position:absolute;width:100%;height:7px;left:0;right:0;top:228%;z-index:1;transform:rotate(90deg)}}";

const CardsDetails = /*@__PURE__*/ proxyCustomElement(class extends HTMLElement {
  constructor() {
    super();
    this.__registerHost();
    this.__attachShadow();
  }
  render() {
    return (h("div", { class: "detalles" }, h("h2", null, "Advanced Statistics"), h("h3", null, "Track how your links are performing across the web with our advanced statistics dashboard"), h("div", { class: "card-inline" }, h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta1" })), h("hr", { class: "linea" }), h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta2" })), h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta3" })))));
  }
  static get style() { return cardsDetailsCss; }
}, [1, "mc-cards-details"]);
function defineCustomElement$1() {
  if (typeof customElements === "undefined") {
    return;
  }
  const components = ["mc-cards-details"];
  components.forEach(tagName => { switch (tagName) {
    case "mc-cards-details":
      if (!customElements.get(tagName)) {
        customElements.define(tagName, CardsDetails);
      }
      break;
  } });
}

const McCardsDetails = CardsDetails;
const defineCustomElement = defineCustomElement$1;

export { McCardsDetails, defineCustomElement };
