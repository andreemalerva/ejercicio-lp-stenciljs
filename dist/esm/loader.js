import { p as promiseResolve, b as bootstrapLazy } from './index-9dd9070a.js';

/*
 Stencil Client Patch Esm v2.15.2 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return bootstrapLazy([["mc-cards-details_7",[[1,"mc-cards-details"],[1,"mc-cards-details-tarjeta",{"icono":[1],"titulo":[1],"parrafo":[1]}],[1,"mc-componente-link"],[1,"mc-footer"],[1,"mc-get-started"],[1,"mc-header"],[1,"mc-navegacion",{"opened":[1540]}]]]], options);
  });
};

export { defineCustomElements };
