import { r as registerInstance, h } from './index-9dd9070a.js';

const cardsDetailsCss = ".detalles{background-color:#f0f1f6;text-align:center;padding:13% 10% 8%;margin-top:80px}.detalles h2{font-family:var(--font1);font-size:40px;line-height:36px;font-weight:bold;color:#34313c;margin-block-end:0}.detalles h3{font-family:var(--font1);font-size:20px;line-height:36px;font-weight:400;color:#a1a1a3;text-align:center;padding:0 20%}.card-inline{display:flex;justify-content:space-between;align-items:center;gap:10px}.card-inline .linea{position:absolute;width:40%;height:7px;background-color:#2AD0D3;left:0;right:0;border:1px solid #2AD0D3;z-index:1}.tarjetas{background-color:#fff;width:27%;text-align:left;border-radius:10px;padding:12px 2%;z-index:10000}.tarjetas:nth-of-type(2){margin-top:6%}.tarjetas:nth-of-type(3){margin-top:12%}@media screen and (max-width: 1024px){.detalles h2{font-size:20px;line-height:32px}.detalles h3{font-size:18px;line-height:26px}}@media screen and (max-width: 425px){.detalles{padding:25% 15px}.card-inline{flex-direction:column}.tarjetas{width:90%;text-align:center;padding:12px 4%;margin:14% 15px}.tarjetas:nth-of-type(2){margin-top:0}.tarjetas:nth-of-type(3){margin-top:0}.card-inline .linea{position:absolute;width:100%;height:7px;left:0;right:0;top:228%;z-index:1;transform:rotate(90deg)}}";

const CardsDetails = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "detalles" }, h("h2", null, "Advanced Statistics"), h("h3", null, "Track how your links are performing across the web with our advanced statistics dashboard"), h("div", { class: "card-inline" }, h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta1" })), h("hr", { class: "linea" }), h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta2" })), h("div", { class: "tarjetas" }, h("slot", { name: "tarjeta3" })))));
  }
};
CardsDetails.style = cardsDetailsCss;

const tarjetaCss = ".contenido .icon{width:80px;height:80px;border-radius:50%;background-color:#3a3053;display:flex;justify-content:center;align-items:center;margin:-50px auto 0}.contenido img{width:40px;height:40px;vertical-align:middle}.contenido h4{font-family:var(--font1);font-size:20px;line-height:24px;font-weight:bold;color:#34313c;margin-block-end:0.33em}.contenido p{font-family:var(--font1);font-size:16px;line-height:28px;font-weight:400;color:#a1a1a3}";

const Tarjeta = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "contenido" }, h("div", { class: "icon" }, h("img", { src: this.icono, alt: "icono" })), h("h4", null, this.titulo), h("p", null, this.parrafo)));
  }
};
Tarjeta.style = tarjetaCss;

const componenteLinkCss = ".link-boton{background-image:url('../assets/images/bg-boost-desktop.svg');background-color:#3a3053;background-size:cover;background-position:center center;padding-bottom:5%}.link-boton h2{padding:4% 0 3%;margin-block-start:0;margin-block-end:0;text-align:center;font-family:var(--font1);font-size:40px;line-height:36px;font-weight:bold;color:#fff}.link-boton a{border-radius:30px;padding:12px 0;background-color:#2ad0d3;display:flex;justify-content:center;align-items:center;width:15%;font-family:var(--font1);font-size:20px;font-weight:bold;color:#fff;text-decoration:none;margin:0 auto}@media screen and (max-width: 980px){.link-boton a{width:40%}}@media screen and (max-width: 425px){.link-boton{padding-top:10%;padding-bottom:12%}.link-boton a{width:40%;margin:10% auto 5%}}";

const ComponenteLink = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "link-boton" }, h("h2", null, " Boost your links today "), h("a", { href: "#" }, "Get Started")));
  }
};
ComponenteLink.style = componenteLinkCss;

const footerCss = ".footer{background-color:#232027;display:flex;justify-content:space-between;align-items:flex-start;padding:4% 10%}.links-footer{display:flex}.footer ul{list-style:none;margin-block-start:0}.footer ul p{color:#fff;font-size:16px;line-height:28px;font-weight:bold;font-family:var(--font1);margin-block-start:0}.footer ul li a{color:#a1a1a3;font-size:14px;line-height:30px;font-weight:300;font-family:var(--font1);text-decoration:none}.redes-sociales{display:flex;gap:25px}@media screen and (max-width: 980px){.footer{padding:4% 15px}.redes-sociales{display:flex;gap:15px}}@media screen and (max-width: 767px){.footer{flex-direction:column;padding:14% 15px;justify-content:center;align-items:center}.footer ul{padding-inline-start:0}.links-footer{flex-direction:column;text-align:center;margin:12% 0}.redes-sociales{display:flex;gap:15px}}";

const Footer = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "footer" }, h("slot", { name: "logotipo" }), h("div", { class: "links-footer" }, h("ul", null, h("p", null, "Features"), h("li", null, h("a", { href: "#" }, "Link Shortening")), h("li", null, h("a", { href: "#" }, "Branded Links")), h("li", null, h("a", { href: "#" }, "Analytics"))), h("ul", null, h("p", null, "Resources"), h("li", null, h("a", { href: "#" }, "Blog")), h("li", null, h("a", { href: "#" }, "Developers")), h("li", null, h("a", { href: "#" }, "Support"))), h("ul", null, h("p", null, "Company"), h("li", null, h("a", { href: "#" }, "About")), h("li", null, h("a", { href: "#" }, "Our Team")), h("li", null, h("a", { href: "#" }, "Careers")), h("li", null, h("a", { href: "#" }, "Contact")))), h("div", { class: "redes-sociales" }, h("slot", { name: "facebook" }), h("slot", { name: "twitter" }), h("slot", { name: "pinteres" }), h("slot", { name: "instagram" }))));
  }
};
Footer.style = footerCss;

const getStartedCss = ".caja-component{background-image:url('../assets/images/bg-shorten-desktop.svg');background-color:#3A3053;background-size:cover;background-position:center center;margin:0 10%;height:150px;border-radius:10px;display:flex;justify-content:center;align-items:center;gap:25px;position:absolute;right:0;left:0;z-index:1000}.caja-component input{margin-left:3.5%;border-radius:10px;padding:12px 16px;background-color:#ffff;width:90%;border:1px solid #fff;color:#a1a1a3;font-family:var(--font1);font-size:17px;font-weight:200}.caja-component input::placeholder{color:#a1a1a3;font-family:var(--font1);font-size:17px;font-weight:200}.caja-component input:focus{outline:none}.caja-component a{margin-right:3.5%;border-radius:10px;padding:12px 16px;background-color:#2AD0D3;width:10%;font-family:var(--font1);font-size:17px;font-weight:bold;color:#fff;text-decoration:none;text-align:center}@media screen and (max-width: 1024px){.caja-component{margin:0 15px}.caja-component input{width:80%}.caja-component a{width:20%}}@media screen and (max-width: 767px){.caja-component{margin:0 15px;gap:15px}.caja-component input{width:60%}.caja-component input::placeholder{font-size:12px;font-weight:200}.caja-component a{width:40%;padding:13px 5px;font-size:14px}}@media screen and (max-width: 425px){.caja-component{flex-flow:wrap;padding:15px;gap:0;height:136px}.caja-component input{width:100%;margin-left:0}.caja-component input::placeholder{font-size:12px;font-weight:200}.caja-component a{width:100%;padding:13px 5px;font-size:14px;margin-right:0}}";

const GetStarted = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "caja-component" }, h("input", { type: "text", placeholder: "Shorten a link here..." }), h("a", { href: "#" }, "Shorten In!")));
  }
};
GetStarted.style = getStartedCss;

const headerCss = "main{display:flex;justify-content:center;align-items:center;padding:0 0 0 10%;height:70vh}main h1{font-family:var(--font1);font-size:82px;line-height:96px;font-weight:bold;color:#34313c;margin-block-start:0;margin-block-end:0.5rem}main h2{font-family:var(--font1);font-size:24px;line-height:36px;font-weight:400;color:#a1a1a3;margin-block-start:0;margin-block-end:0;margin-bottom:7%}main a{background-color:#2ad0d3;border-radius:35px;color:#fff;font-size:20px;line-height:20px;font-family:var(--font1);text-transform:capitalize;padding:16px 0;text-decoration:none;display:flex;justify-content:center;align-items:center;width:30%}.derecha{background-image:url('./assets/images/illustration-working.svg');background-size:cover;height:60vh;width:100%}@media screen and (max-width: 1024px){main h1{font-size:42px;line-height:46px}main h2{font-size:18px;line-height:32px}main a{font-size:18px;line-height:20px;width:35%}}@media screen and (max-width: 920px){main{padding:0}main .izquierda{padding:0 15px}}@media screen and (max-width: 767px){main{flex:1;flex-flow:column wrap;height:100vh;justify-content:flex-start;align-items:flex-start}main h1{font-size:42px;line-height:46px;text-align:center}main h2{font-size:22px;line-height:30px;text-align:center}main a{font-size:18px;line-height:20px;margin:0 auto}main .izquierda{flex:1;order:2;padding:0 15px;margin:5% 0}main .derecha{order:1;height:300px}}@media screen and (max-width: 374px){main .derecha{height:250px}main a{width:50%}}";

const Header = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("main", null, h("div", { class: "izquierda" }, h("h1", null, "More than just shorter links"), h("h2", null, "Build your brand\u2019s recognition and get detailed insights on how your links are performing."), h("a", { href: "#" }, "Get Started")), h("div", { class: "derecha" })));
  }
};
Header.style = headerCss;

const navegacionCss = ":host(){margin:0}.topnav{overflow:hidden;background-color:#ffff;display:flex;justify-content:space-between;padding:2.5% 10% 1%}.topnav a{padding:14px 0;font-family:var(--font1);font-size:17px;font-weight:bold;color:#a1a1a3;text-decoration:none}.topnav a img{vertical-align:middle}.nav-izquierda{display:flex;align-items:center;justify-content:space-between;gap:30px}.menuContent{display:flex;align-items:center;justify-content:space-between;gap:25px}.menuContent a:hover{background-color:rgba(221, 221, 221, 0);color:black}.menuContent a.active{background-color:#f0e914;color:white}.menuContent .icon{display:none}#contacto{background-color:#2AD0D3;border-radius:35px;color:#fff;font-family:var(--font1);text-transform:capitalize;padding:8px 20px}@media screen and (max-width: 600px){.menuContent a,.menuContent a:not(:first-child){display:none}.menuContent a.icon{float:right;display:block}.menuContent .icon:hover{background-color:#000000}.menuContent .icon:active{background-color:#000000}.menuContent.responsive{position:absolute;background-color:#000000;display:inline-block;width:100vw;text-align:center}.menuContent.responsive .icon{position:absolute;right:0;top:0}.menuContent.responsive a{float:none;display:block;text-align:left;color:rgb(255, 255, 255)}.fa-navicon:before,.fa-reorder:before,.fa-bars:before{content:\"\\f0c9\";color:white}}";

const Navegacion = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h("div", { class: "topnav" }, h("div", { class: "nav-izquierda" }, h("a", { href: "#home" }, h("img", { src: "./assets/images/logo.svg", alt: "Logotipo" })), h("a", { href: "#about" }, "Feature"), h("a", { href: "#about" }, "Pricing"), h("a", { href: "#services" }, "Resources")), h("div", { class: "menuContent", id: "myTopnav" }, h("a", { href: "#proyects" }, "Login"), h("a", { href: "#contact", id: "contacto" }, "Sign Up"), h("a", { href: "#", class: "icon" }, h("i", { class: "fa fa-bars" })))));
  }
};
Navegacion.style = navegacionCss;

export { CardsDetails as mc_cards_details, Tarjeta as mc_cards_details_tarjeta, ComponenteLink as mc_componente_link, Footer as mc_footer, GetStarted as mc_get_started, Header as mc_header, Navegacion as mc_navegacion };
