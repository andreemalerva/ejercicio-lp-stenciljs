import { Component, h } from "@stencil/core";
export class Footer {
  render() {
    return (h("div", { class: "footer" },
      h("slot", { name: "logotipo" }),
      h("div", { class: "links-footer" },
        h("ul", null,
          h("p", null, "Features"),
          h("li", null,
            h("a", { href: "#" }, "Link Shortening")),
          h("li", null,
            h("a", { href: "#" }, "Branded Links")),
          h("li", null,
            h("a", { href: "#" }, "Analytics"))),
        h("ul", null,
          h("p", null, "Resources"),
          h("li", null,
            h("a", { href: "#" }, "Blog")),
          h("li", null,
            h("a", { href: "#" }, "Developers")),
          h("li", null,
            h("a", { href: "#" }, "Support"))),
        h("ul", null,
          h("p", null, "Company"),
          h("li", null,
            h("a", { href: "#" }, "About")),
          h("li", null,
            h("a", { href: "#" }, "Our Team")),
          h("li", null,
            h("a", { href: "#" }, "Careers")),
          h("li", null,
            h("a", { href: "#" }, "Contact")))),
      h("div", { class: "redes-sociales" },
        h("slot", { name: "facebook" }),
        h("slot", { name: "twitter" }),
        h("slot", { name: "pinteres" }),
        h("slot", { name: "instagram" }))));
  }
  static get is() { return "mc-footer"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./footer.css"]
  }; }
  static get styleUrls() { return {
    "$": ["footer.css"]
  }; }
}
