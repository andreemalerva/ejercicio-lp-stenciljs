import { Component, h } from "@stencil/core";
export class GetStarted {
  render() {
    return (h("div", { class: "caja-component" },
      h("input", { type: "text", placeholder: "Shorten a link here..." }),
      h("a", { href: "#" }, "Shorten In!")));
  }
  static get is() { return "mc-get-started"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./get-started.css"]
  }; }
  static get styleUrls() { return {
    "$": ["get-started.css"]
  }; }
}
