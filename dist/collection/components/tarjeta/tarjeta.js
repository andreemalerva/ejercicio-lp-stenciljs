import { Component, h, Prop } from '@stencil/core';
export class Tarjeta {
  render() {
    return (h("div", { class: "contenido" },
      h("div", { class: "icon" },
        h("img", { src: this.icono, alt: "icono" })),
      h("h4", null, this.titulo),
      h("p", null, this.parrafo)));
  }
  static get is() { return "mc-cards-details-tarjeta"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./tarjeta.css"]
  }; }
  static get styleUrls() { return {
    "$": ["tarjeta.css"]
  }; }
  static get properties() { return {
    "icono": {
      "type": "string",
      "mutable": false,
      "complexType": {
        "original": "string",
        "resolved": "string",
        "references": {}
      },
      "required": false,
      "optional": false,
      "docs": {
        "tags": [],
        "text": ""
      },
      "attribute": "icono",
      "reflect": false
    },
    "titulo": {
      "type": "string",
      "mutable": false,
      "complexType": {
        "original": "string",
        "resolved": "string",
        "references": {}
      },
      "required": false,
      "optional": false,
      "docs": {
        "tags": [],
        "text": ""
      },
      "attribute": "titulo",
      "reflect": false
    },
    "parrafo": {
      "type": "string",
      "mutable": false,
      "complexType": {
        "original": "string",
        "resolved": "string",
        "references": {}
      },
      "required": false,
      "optional": false,
      "docs": {
        "tags": [],
        "text": ""
      },
      "attribute": "parrafo",
      "reflect": false
    }
  }; }
}
