import { Component, h } from "@stencil/core";
export class Header {
  render() {
    return (h("main", null,
      h("div", { class: "izquierda" },
        h("h1", null, "More than just shorter links"),
        h("h2", null, "Build your brand\u2019s recognition and get detailed insights on how your links are performing."),
        h("a", { href: "#" }, "Get Started")),
      h("div", { class: "derecha" })));
  }
  static get is() { return "mc-header"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./header.css"]
  }; }
  static get styleUrls() { return {
    "$": ["header.css"]
  }; }
}
