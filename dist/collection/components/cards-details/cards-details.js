import { Component, h } from '@stencil/core';
export class CardsDetails {
  render() {
    return (h("div", { class: "detalles" },
      h("h2", null, "Advanced Statistics"),
      h("h3", null, "Track how your links are performing across the web with our advanced statistics dashboard"),
      h("div", { class: "card-inline" },
        h("div", { class: "tarjetas" },
          h("slot", { name: "tarjeta1" })),
        h("hr", { class: "linea" }),
        h("div", { class: "tarjetas" },
          h("slot", { name: "tarjeta2" })),
        h("div", { class: "tarjetas" },
          h("slot", { name: "tarjeta3" })))));
  }
  static get is() { return "mc-cards-details"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./cards-details.css"]
  }; }
  static get styleUrls() { return {
    "$": ["cards-details.css"]
  }; }
}
