import { Component, h } from "@stencil/core";
export class ComponenteLink {
  render() {
    return (h("div", { class: "link-boton" },
      h("h2", null, " Boost your links today "),
      h("a", { href: "#" }, "Get Started")));
  }
  static get is() { return "mc-componente-link"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./componente-link.css"]
  }; }
  static get styleUrls() { return {
    "$": ["componente-link.css"]
  }; }
}
