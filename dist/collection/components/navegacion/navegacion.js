import { Component, h, Prop } from "@stencil/core";
export class Navegacion {
  render() {
    return (h("div", { class: "topnav" },
      h("div", { class: "nav-izquierda" },
        h("a", { href: "#home" },
          h("img", { src: "./assets/images/logo.svg", alt: "Logotipo" })),
        h("a", { href: "#about" }, "Feature"),
        h("a", { href: "#about" }, "Pricing"),
        h("a", { href: "#services" }, "Resources")),
      h("div", { class: "menuContent", id: "myTopnav" },
        h("a", { href: "#proyects" }, "Login"),
        h("a", { href: "#contact", id: "contacto" }, "Sign Up"),
        h("a", { href: "#", class: "icon" },
          h("i", { class: "fa fa-bars" })))));
  }
  static get is() { return "mc-navegacion"; }
  static get encapsulation() { return "shadow"; }
  static get originalStyleUrls() { return {
    "$": ["./navegacion.css"]
  }; }
  static get styleUrls() { return {
    "$": ["navegacion.css"]
  }; }
  static get properties() { return {
    "opened": {
      "type": "boolean",
      "mutable": true,
      "complexType": {
        "original": "boolean",
        "resolved": "boolean",
        "references": {}
      },
      "required": false,
      "optional": false,
      "docs": {
        "tags": [],
        "text": ""
      },
      "attribute": "opened",
      "reflect": true
    }
  }; }
}
