'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-f62dca13.js');

/*
 Stencil Client Patch Esm v2.15.2 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return index.promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return index.bootstrapLazy([["mc-cards-details_7.cjs",[[1,"mc-cards-details"],[1,"mc-cards-details-tarjeta",{"icono":[1],"titulo":[1],"parrafo":[1]}],[1,"mc-componente-link"],[1,"mc-footer"],[1,"mc-get-started"],[1,"mc-header"],[1,"mc-navegacion",{"opened":[1540]}]]]], options);
  });
};

exports.defineCustomElements = defineCustomElements;
