'use strict';

const index = require('./index-f62dca13.js');

/*
 Stencil Client Patch Browser v2.15.2 | MIT Licensed | https://stenciljs.com
 */
const patchBrowser = () => {
    const importMeta = (typeof document === 'undefined' ? new (require('u' + 'rl').URL)('file:' + __filename).href : (document.currentScript && document.currentScript.src || new URL('ejercicios-lp.cjs.js', document.baseURI).href));
    const opts = {};
    if (importMeta !== '') {
        opts.resourcesUrl = new URL('.', importMeta).href;
    }
    return index.promiseResolve(opts);
};

patchBrowser().then(options => {
  return index.bootstrapLazy([["mc-cards-details_7.cjs",[[1,"mc-cards-details"],[1,"mc-cards-details-tarjeta",{"icono":[1],"titulo":[1],"parrafo":[1]}],[1,"mc-componente-link"],[1,"mc-footer"],[1,"mc-get-started"],[1,"mc-header"],[1,"mc-navegacion",{"opened":[1540]}]]]], options);
});
