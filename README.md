<div>
    <h1>Shortly - STENCIL JS</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/ejercicio-lp-stenciljs), actualmente soy Desarrollador Front End, y me agrada seguir aprendiento.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 2283530727
```

# Acerca del proyecto

Este es un proyecto realizado con componentes en Stencil JS.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://lp-stenciljs.netlify.app/)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2023 LYAM ANDREE CRUZ MALERVA
