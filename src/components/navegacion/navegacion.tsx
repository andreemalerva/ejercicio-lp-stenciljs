import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: 'mc-navegacion',
  styleUrl:'./navegacion.css',
  shadow: true
})
export class Navegacion{
  @Prop({ reflect: true, mutable: true }) opened: boolean;


  render(){
    return(
      <div class="topnav">
        <div class="nav-izquierda">
          <a href="#home"><img src="./assets/images/logo.svg" alt="Logotipo"/></a>
          <a href="#about">Feature</a>
          <a href="#about">Pricing</a>
          <a href="#services">Resources</a>
        </div>

      <div class="menuContent" id="myTopnav">
        <a href="#proyects">Login</a>
        <a href="#contact" id="contacto">Sign Up</a>
        <a href="#" class="icon">
          <i class="fa fa-bars"></i>
        </a>
      </div>
    </div>
    )
  }
}
