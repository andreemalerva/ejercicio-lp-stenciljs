import { Component, h } from "@stencil/core";

@Component({
  tag: 'mc-footer',
  styleUrl:'./footer.css',
  shadow: true
})
export class Footer{
  render(){
    return(
      <div class="footer">
        <slot name="logotipo"/>
        <div class="links-footer">
          <ul>
            <p>Features</p>
            <li><a href="#">Link Shortening</a></li>
            <li><a href="#">Branded Links</a></li>
            <li><a href="#">Analytics</a></li>
          </ul>
          <ul>
            <p>Resources</p>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Developers</a></li>
            <li><a href="#">Support</a></li>
          </ul>
          <ul>
            <p>Company</p>
            <li><a href="#">About</a></li>
            <li><a href="#">Our Team</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </div>
        <div class="redes-sociales">
          <slot name="facebook"></slot>
          <slot name="twitter"></slot>
          <slot name="pinteres"></slot>
          <slot name="instagram"></slot>
        </div>
      </div>
    )
  }
}
