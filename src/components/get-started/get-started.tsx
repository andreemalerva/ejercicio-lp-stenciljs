import { Component, h } from "@stencil/core";

@Component({
  tag: 'mc-get-started',
  styleUrl: './get-started.css',
  shadow: true
})
export class GetStarted {

  render() {
    return (
        <div class="caja-component">
          <input type="text" placeholder="Shorten a link here..." />
          <a href="#">Shorten In!</a>
        </div>
    )
  }
}
