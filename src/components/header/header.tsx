import { Component, h } from "@stencil/core";

@Component({
  tag: 'mc-header',
  styleUrl: './header.css',
  shadow: true
})
export class Header {
  render() {
    return (
      <main>
        <div class="izquierda">
          <h1>
            More than just shorter links
          </h1>
          <h2>Build your brand’s recognition and get detailed insights
            on how your links are performing.</h2>
          <a href="#">Get Started</a>
        </div>
        <div class="derecha"></div>
      </main>
    )
  }
}
