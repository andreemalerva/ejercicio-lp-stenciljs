import { Component, h } from "@stencil/core";

@Component({
  tag: 'mc-componente-link',
  styleUrl:'./componente-link.css',
  shadow: true
})
export class ComponenteLink{
  render(){
    return(
      <div class="link-boton">
        <h2> Boost your links today </h2>
        <a href="#">Get Started</a>
      </div>
    )
  }
}
