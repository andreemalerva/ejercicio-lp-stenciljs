import { Component, h } from '@stencil/core';

@Component({
  tag: 'mc-cards-details',
  styleUrl: './cards-details.css',
  shadow: true,
})
export class CardsDetails {
  render() {
    return (
      <div class="detalles">
        <h2>Advanced Statistics</h2>
        <h3>Track how your links are performing across the web with our advanced statistics dashboard</h3>
        <div class="card-inline">
          <div class="tarjetas">
            <slot name="tarjeta1"></slot>
          </div>
          <hr class="linea" />
          <div class="tarjetas">
            <slot name="tarjeta2"></slot>
          </div>
          <div class="tarjetas">
            <slot name="tarjeta3"></slot>
          </div>
        </div>
      </div>
    );
  }
}
