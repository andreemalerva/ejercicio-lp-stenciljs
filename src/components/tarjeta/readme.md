# mc-cards-details-tarjeta



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type     | Default     |
| --------- | --------- | ----------- | -------- | ----------- |
| `icono`   | `icono`   |             | `string` | `undefined` |
| `parrafo` | `parrafo` |             | `string` | `undefined` |
| `titulo`  | `titulo`  |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
