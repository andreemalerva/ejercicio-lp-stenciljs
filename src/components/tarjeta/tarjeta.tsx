import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'mc-cards-details-tarjeta',
  styleUrl: './tarjeta.css',
  shadow: true,
})
export class Tarjeta {
  @Prop() icono: string;
  @Prop() titulo: string;
  @Prop() parrafo: string;

  render() {
    return (
      <div class="contenido">
        <div class="icon">
          <img src={this.icono} alt="icono" />
        </div>
        <h4>{this.titulo}</h4>
        <p>{this.parrafo}</p>
      </div>
    );
  }
}
